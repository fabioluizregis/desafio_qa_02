Instruções para execução:

Após ter o todo o ambiente preparado, executar os comandos:

> bundle install

> cucumber => Para  rodar todo o projeto desta pasta
> cucumber -t <TAG> => Para rodar apenas a parte do projeto referente à <TAG>:
  <TAG> = @cadastro_admin => Para o cadastro de um novo customer
  <TAG> = @login_admin => Para verificação do login como administrador
  <TAG> = @pesquisa_aerea => Para pesquisa de passagem aérea