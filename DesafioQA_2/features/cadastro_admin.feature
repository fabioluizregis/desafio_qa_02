#language:pt

Funcionalidade: Cadastro de usuários
    Eu como um usuário do sistema
    Quero logar como administrador
    Para cadastrar um novo usuario customer

    @cadastro_admin
    Cenário: Cadastro de novo customer

        Dado que estou na página de login admin
        Quando eu faço login admin com "admin@phptravels.com" e "demoadmin"
        E clico no botão customer
        Então posso cadastrar um novo customer
        E verifico que o novo customer foi cadastrado com sucesso
