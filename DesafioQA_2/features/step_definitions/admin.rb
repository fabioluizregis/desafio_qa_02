Dado("que estou na página de login admin") do
    @login_admin.load
  end
  
  Quando("eu faço login admin com {string} e {string}") do |email, password|
    @login_admin.logar(email,password)
    @email_usuario = email
  end
  
  Então("sou autenticado com sucesso") do
    @login_admin.botao_profile.click
    expect(@login_admin.campo_profile_email.value).to eql @email_usuario
  end

  Quando("clico no botão customer") do
    @login_admin.botao_accounts.click
    @login_admin.botao_customers.click
  end
  
  Então("posso cadastrar um novo customer") do
    @login_admin.botao_add_customer.click

    @first_name = Faker::Name.first_name
    @last_name = Faker::Name.last_name

    @email_cadastro = "#{@first_name}.#{@last_name}@desafio.inm"
    @email_cadastro = @email_cadastro.downcase
    @senha = "123456"


    @login_admin.customer_firstname.set @first_name
    @login_admin.customer_lastname.set @last_name
    @login_admin.customer_email.set @email_cadastro
    @login_admin.customer_senha.set @senha

    @login_admin.customer_mobile.set Faker::PhoneNumber.cell_phone

    @login_admin.seleciona_pais("Brazil")
    # seleciona_pais(Faker::Address.country)
    #ver .sample

    @login_admin.customer_address1.set Faker::Address.full_address
    @login_admin.customer_address2.set Faker::Address.full_address

    # Se o randomico retornar 0, muda o status, senão não faz nada
    @login_admin.customer_status.select "Disable" if rand(2) == 0

    # Se o randomico retornar 0, clica no OptIn, senão não faz nada
    @login_admin.customer_optin.click if rand(2) == 0

    @login_admin.customer_botao_submit.click

   end
  
  Então("verifico que o novo customer foi cadastrado com sucesso") do
    @login_admin.botão_search_customer.click
    @login_admin.campo_busca_usuario.set @email_cadastro
    @login_admin.lista_tipo_busca.select "Email"
    @login_admin.botao_go.click

    expect(@login_admin.tabela_resultados_customers[0].text).to eql("#{@email_cadastro}")
  end