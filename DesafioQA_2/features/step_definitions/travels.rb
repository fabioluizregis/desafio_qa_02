  Dado("que estou na página de login de customer") do
    @travels_page.load
  end
  
  Quando("eu faço login customer com {string} e {string}") do |email, password|
    @travels_page.logar(email,password)
  end

  Quando("navego até a página de pesquisa de vôos") do

    sleep(5) #wait_until(element)
    #@travels_page.wait_for_barra_menu
    @travels_page.barra_menu[1].click

    
  end
  
  Quando("pesquiso um destino de viagem") do
    
    
    @travels_page.campo_destinos.send_keys "MIA"
    sleep(1)

    @travels_page.seletor_destinos[0].click

  end
  
  Quando("o mês de pesquisa não pode ser mês de alta procura") do

    
    @hoje = Date.today
    @calculo_mes = @hoje >> 3

    if @calculo_mes.month == 12
      @calculo_mes = @calculo_mes >> 3

      elsif @calculo_mes.month == 01
        @calculo_mes = @calculo_mes >> 2
      
      elsif @calculo_mes.month == 02 || @calculo_mes.month == 07
        @calculo_mes = @calculo_mes >> 1

    end

    if @calculo_mes.month == 03 || @calculo_mes.month == 05 || @calculo_mes.month.to_s == "08" || @calculo_mes.month == 10
      @dia = rand(1..31)
    else
      @dia = rand(1..30)
    end

    if @dia <= 9
      @dia_final = "0#{@dia.to_s}"
      else
        @dia_final = "#{@dia.to_s}"
    end

    if @calculo_mes.month < 10
      @mes_final = "0#{@calculo_mes.month.to_s}"
    end

    @travels_page.campo_data_partida.click
    @travels_page.campo_data_partida.set "#{@calculo_mes.year.to_s}-#{@mes_final}-#{@dia_final}"
    
  end
  
  Quando("o período de busca deve ser para daqui {int} meses") do |intervalo_busca|

    verificar_mes = Date.today
    verificar_mes = verificar_mes  >> intervalo_busca 
    expect(verificar_mes.month).to eql @calculo_mes.month

  end
  
  Então("a pesquisa em retorna resultados sobre o destino pesquisado") do
    @travels_page.botao_search.click
    
    expect(@travels_page.resultado_busca[0]).to have_content('MIA')
    #sleep(5)
  end