require 'capybara'
require 'capybara/cucumber'
require 'date'
require 'faker'
require 'pry'
require 'site_prism'


Capybara.configure do |config|
    config.default_driver = :selenium_chrome

    config.app_host = 'https://www.phptravels.net'
end

Capybara.default_max_wait_time = 10