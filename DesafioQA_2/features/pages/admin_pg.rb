class AdminPage < SitePrism::Page
    set_url '/admin'

    #Elementos da página de login
    element :campo_email, 'input[name=email]'
    element :campo_password, 'input[name=password]'
    element :botao_login, 'button[type=submit]'

    #Elementos da página principal
    element :botao_profile, '.btn-success.btn-sm'

    element :botao_accounts, 'a[href="#ACCOUNTS"]'
    element :botao_customers, 'a[href$="/accounts/customers/"]'

    #elements :tabela_resultados_customers, '.xcrud-list-container'
    elements :tabela_resultados_customers, 'tr[class="xcrud-row xcrud-row-0"] td a'

    

    element :botao_add_customer, 'button[class="btn btn-success"]'
    element :botão_search_customer, '.xcrud-search-toggle'
    element :campo_busca_usuario, 'input[class="xcrud-searchdata xcrud-search-active input-small form-control"]'
    element :lista_tipo_busca, 'select[class="xcrud-data xcrud-columns-select input-small form-control"]'
    element :botao_go, 'a[class="xcrud-action btn btn-primary"]'

    #Elementos da página de profile
    element :campo_profile_email, 'input[name="email"]'

    #Elementos da página de cadastro de novo customer
    element :customer_firstname, 'input[name="fname"]'
    element :customer_lastname, 'input[name="lname"]'
    element :customer_email, 'input[name="email"]'
    element :customer_senha, 'input[placeholder="Password"]'
    element :customer_mobile, 'input[name="mobile"]'
    element :customer_address1, 'input[name="address1"]'
    element :customer_address2, 'input[name="address2"]'
    element :customer_status, 'select[name="status"]'
    element :customer_botao_submit, 'button[class="btn btn-primary"]'
    element :customer_optin, '.icheckbox_square-grey'




    def logar(email, password)
        campo_email.set email
        campo_password.set password
        botao_login.click
    end

    def seleciona_pais(pais_desejado)
        find('#s2id_autogen1').click
        find('#s2id_autogen1 + select option' , text: pais_desejado).click
    end


end