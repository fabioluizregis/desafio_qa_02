class TravelsPage < SitePrism::Page
    set_url '/login'

    #Página de Login
    element :campo_email, 'input[name="username"]'
    element :campo_password, 'input[name="password"]' 
    element :botao_login, '.loginbtn'

    elements :barra_menu, '.main-lnk'

    #Página flights
    element :campo_destinos, '#s2id_autogen1'
    elements :seletor_destinos, '.select2-results > li'
    element :campo_data_partida, 'input[placeholder="Depart"]'
    element :botao_search, 'div[class="bgfade col-md-1 col-xs-12 search-button"]'
    elements :resultado_busca, 'div[class="col-md-4 col-sm-3 col-xs-4"]'


    def logar(email, password)
        campo_email.set email
        campo_password.set password
        botao_login.click
    end

end