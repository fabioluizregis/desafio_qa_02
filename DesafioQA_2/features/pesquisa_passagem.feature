#language:pt

Funcionalidade: Disponibilidade de passagens
    Eu como um usuário do sistema
    Quero pesquisar passagens aéreas    
    Para verificar a disponibilidade de passagens

    @pesquisa_aerea
    Cenário: Pesquisa de passagens para daqui 3 meses

        Dado que estou na página de login de customer
        Quando eu faço login customer com "user@phptravels.com" e "demouser"
        E navego até a página de pesquisa de vôos
        E pesquiso um destino de viagem
        E o mês de pesquisa não pode ser mês de alta procura
        Mas o período de busca deve ser para daqui 3 meses
        Então a pesquisa em retorna resultados sobre o destino pesquisado
