#language:pt

Funcionalidade: Login
    Eu como um usuário do sistema
    Quero logar como administrador
    Para realizar tarefas administrativas

    @login_admin
    Cenário: Login com usuário admin

        Dado que estou na página de login admin
        Quando eu faço login admin com "admin@phptravels.com" e "demoadmin"
        Então sou autenticado com sucesso
