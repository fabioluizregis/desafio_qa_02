![Alt text](inmetrics.png)

# **Desafio QA**

---

## Introdução

Automatizar os testes funcionais que são planejados durante a Sprint é umas das muitas atribuições do *Agile Tester*.

Hoje, é comum as aplicações serem desenvolvidas para diversas plataformas (Android, iOS, Website) e consumirem uma webservice para adicionar uma camada de segurança, redução de código, entre outros benefícios.

Deste modo, além de realizar testes de *front* em websites ou no app mobile, é importante adicionar testes de serviço para obter maior qualidade no produto desenvolvido.

---

## Objetivo

Avaliar as competências do candidato no entendimento e escrita de Cenários e na construção de testes automatizados, bem como no uso de boas práticas.

---

## Tecnologias

* *Para documentar os cenários especificados seguindo BDD*: **Cucumber**
* *Linguagem de programação*: **Ruby** ou **Java**
* *Bibliotecas*: as que achar necessário para execução dos testes

---

## Projeto
Criar um **único** projeto contendo:

* ***"Readme"*** com orientações de como instalar o ambiente e executar os testes
* A solução dos desafios propostos a seguir

---

## Desafios - WebSite
1. Fazer o cadastro de um novo *Customer* no site [https://www.phptravels.net/admin](https://www.phptravels.net/admin) e validar que o mesmo foi adicionado a lista de clientes cadastrados. Dados para login:
>* **usuário**: admin@phptravels.com
>* **senha**: demoadmin

2. Fazer a consulta de uma passagem aérea no site [https://www.phptravels.net](https://www.phptravels.net) e verificar que retornaram resultados conforme a escolha.
Para essa Funcionalidade existem algumas regras que devem ser implementadas no código:
>* O mês escolhido precisa ser daqui a 3 meses do atual e não poderá ser um mês de grande procura (Janeiro, Fevereiro, Julho ou Dezembro). Quanto ao dia, deverá ser gerado aleatoriamente.

**Requerido**: Utilizar conceito de PageObjects e geração dinâmica de dados para efetuar o cadastro

---

## Desafios - WebService
1. Escolher uma das APIs disponíveis em [http://fakerestapi.azurewebsites.net/swagger/ui/index](http://fakerestapi.azurewebsites.net/swagger/ui/index) para enviar uma requisição do tipo **POST**. Validar o status code e o response do serviço

2. Enviar um **GET** para [https://jsonplaceholder.typicode.com/todos](https://jsonplaceholder.typicode.com/todos), validar o status code e exibir somente o *id / title* dos itens que estão como *"completed: true"*

**Requerido**: Utilizar conceito de ServiceObjects e geração dinâmica de dados para efetuar o POST

---

## Envio dos Desafios

Primeiramente, efetue um **fork** deste repositório. Desenvolva e faça os *commits* no seu **fork**. Quando terminar, envie um **Pull Request** através do **BitBucket**.

---

## Dica

Caso não consiga finalizar 100% do projeto, nos envie mesmo assim!

Nós avaliamos diversos itens como lógica, estrutura, padrões utilizados, entre outras coisas
