Instruções para execução:

Após ter o todo o ambiente preparado, executar os comandos:

> bundle install

> cucumber => Para  rodar todo o projeto desta pasta
> cucumber -t <TAG> => Para rodar apenas a parte do projeto referente à <TAG>:
  <TAG> = @req_post => Para testar a requisição POST
  <TAG> = @req_get => Para testar a requisição GET