Dado("que eu tenha acesso ao webservice") do
 # pending # Write code here that turns the phrase above into concrete actions
end

Quando("faço uma requisicao GET para o serviço") do
  @requisicao = HTTParty.get("http://jsonplaceholder.typicode.com/todos")
end

Então("o código de resposta GET deve ser {string}") do |codigo_status|
  expect(@requisicao.response.code).to eql codigo_status
end

Então("imprimo ID e TITTLE de todos os itens com status code completed = true") do
 
  @requisicao.each do |cada_item|
    if cada_item['completed'].eql?(true)
      if cada_item['id'] < 100
        puts "ID: #{cada_item['id']} ...... Tittle: #{cada_item['title']}"
      else
        puts "ID: #{cada_item['id']} ..... Tittle: #{cada_item['title']}" 
      end
    end
  end

end
