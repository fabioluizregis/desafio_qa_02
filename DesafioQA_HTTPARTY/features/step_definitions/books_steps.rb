#encoding: utf-8

Dado("que o cliente informou os dados de um novo livro:") do |table|
    @request = table.rows_hash
  end
  
  Quando("faço uma requisicao POST para o serviço BOOKS") do
    @resultado = HTTParty.post(
        'http://fakerestapi.azurewebsites.net/swagger/ui/index#!/Books/Books_Post',
        body: @request.to_json
    )
    
  end
  
  Então("o código de resposta deve ser {string}") do |codigo_status|
    expect(@resultado.response.code).to eql codigo_status
  end
