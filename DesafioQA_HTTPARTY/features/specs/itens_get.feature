#language: pt

Funcionalidade: Exibição de dados
    @req_get
    Cenario: Exibição de id e title para completed=true
      Dado que eu tenha acesso ao webservice
      Quando faço uma requisicao GET para o serviço
      Então o código de resposta GET deve ser "200"
      E imprimo ID e TITTLE de todos os itens com status code completed = true