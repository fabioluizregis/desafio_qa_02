#language: pt

Funcionalidade: Cadastro de livro

    Cenario: Novo livro
    @req_post
    Dado que o cliente informou os dados de um novo livro:
        | ID          | 0                        |
        | Title       | Teste livro Fabio        |
        | Description | Livro do Fabio           |
        | PageCount   | 10                       |
        | Excerpt     | Testando o livro         |
        | PublishDate | 2018-06-17T13:48:20.451Z |
    Quando faço uma requisicao POST para o serviço BOOKS
    Então o código de resposta deve ser "200"